﻿// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(750, 600), L"Первая программа!");//750-ширина,600-высота

    // Создаем треугольник
    sf::CircleShape triangle(65.f, 3);
    triangle.setFillColor(sf::Color::Blue); // устанавливаем цвет треугольника - синий
    int triangle_x = 100, triangle_y = 485;
    triangle.setPosition(triangle_x, triangle_y); // устанавливаем начальную позицию справа от круга
    



    // Создаем квадрат
    sf::CircleShape square(60.f, 4);
    square.setFillColor(sf::Color::Red); // устанавливаем цвет квадрата - красный
    int square_x = 300, square_y = 475;
    square.setPosition(square_x, square_y); // устанавливаем начальную позицию справа от треугольника
    


    // Создаем октагон
    sf::CircleShape octagon(61.f, 8);
    octagon.setFillColor(sf::Color::Cyan); // устанавливаем цвет октагона - бирюзовый
    int octagon_x = 500, octagon_y = 475;
    octagon.setPosition(octagon_x, octagon_y); // устанавливаем начальную позицию справа от квадрата
    




    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }

     
        triangle_y--;
        if (triangle_y < 0)
           triangle_y = 0 ;
        triangle.setPosition(triangle_x, triangle_y);



        square_y--;
        if (square_y < 0)
            square_y = 0;
        square.setPosition(square_x, square_y);



        octagon_y--;
        if (octagon_y < 0)
            octagon_y = 0;
        octagon.setPosition(octagon_x, triangle_y);

        // Очистить окно от всего
        window.clear();

        // Перемещение фигуры в буфер
        window.draw(triangle); 
        window.draw(square);
        window.draw(octagon);

        // Отобразить на окне все, что есть в буфере
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    return 0;
}
